package fr.lyneteam.nico.eventlistener;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class EventUser implements EventListenerUser, EventListener {
	private final Map<EventListenerUser, List<EventListener>> listeners;
	
	public EventUser() {
		this.listeners = new HashMap<EventListenerUser, List<EventListener>>();
		this.addEventListeners(this, this);
	}
	
	public void addEventListeners(EventListenerUser user, EventListener... listeners) {
		if (user == null) try {
			throw new Exception("User cannot be null.");
		} catch (Exception exception) {
			exception.printStackTrace();
			return;
		}
		if (!this.listeners.containsKey(user)) this.listeners.put(user, new ArrayList<EventListener>());
		for (EventListener listener : listeners) if (!(listener == null)) this.listeners.get(user).add(listener);
	}
	
	public void removeEventListeners(EventListenerUser user, EventListener... listeners) {
		for (Entry<EventListenerUser, List<EventListener>> entry : new HashMap<EventListenerUser, List<EventListener>>(this.listeners).entrySet()) if (user == null || entry.getKey().equals(user)) for (EventListener listener : listeners) if (!(listener == null)) this.listeners.get(user).remove(listener);
	}
	
	public void removeEventListeners(EventListener... listeners) {
		this.removeEventListeners(null, listeners);
	}
	
	public void removeEventListeners(EventListenerUser user) {
		if (this.listeners.containsKey(user)) this.listeners.remove(user);
	}
	
	public void callEvent(Event event) {
		try {
			if (!(event == null)) {
				event.before();
				try {
					Map<EventHandler.Priority, List<EventClassMethod>> methods = new HashMap<EventHandler.Priority, List<EventClassMethod>>();
					for (EventHandler.Priority priority : EventHandler.Priority.values()) methods.put(priority, new ArrayList<EventClassMethod>());
					for (List<EventListener> list : new ArrayList<List<EventListener>>(this.listeners.values())) for (EventListener listener : list) {
						Class<? extends EventListener> object = listener.getClass();
						for (Method method : object.getDeclaredMethods()) if (method.isAnnotationPresent(EventHandler.class)) {
							Annotation annotation = method.getAnnotation(EventHandler.class);
							EventHandler handler = (EventHandler) annotation;
							if (method.getParameterTypes().length == 1 && method.getParameterTypes()[0].getName().equals(event.getClass().getName())) methods.get(handler.priority()).add(new EventClassMethod(listener, object, method));
						}
					}
					for (String priority : "HIGHEST HIGH MEDIUM LOW LOWEST".split(" ")) for (EventClassMethod method : methods.get(EventHandler.Priority.valueOf(priority))) try {
						Method cast = method.getMethod();
						cast.setAccessible(true);
						cast.invoke(method.getListener(), event);
					} catch (Exception exception) {
						System.err.println("Error sending event to a listener (" + method.getListener().getClass().getCanonicalName() + " : " + method.getMethod().getName() + ")");
						exception.printStackTrace();
					}
				} catch (Exception exception) {
					System.err.println("Error dispaching events.");
					exception.printStackTrace();
				}
				event.after();
			}
		} catch (Exception exception) {
			System.err.println("Error durring listening event.");
			exception.printStackTrace();
		}
	}
}