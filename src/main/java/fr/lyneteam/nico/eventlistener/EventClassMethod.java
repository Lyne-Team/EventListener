package fr.lyneteam.nico.eventlistener;

import java.lang.reflect.Method;

public class EventClassMethod {
	private final EventListener listener;
	private final Class<? extends EventListener> object;
	private final Method method;

	public EventClassMethod(EventListener listener, Class<? extends EventListener> object, Method method) {
		this.listener = listener;
		this.object = object;
		this.method = method;
	}

	public final EventListener getListener() {
		return this.listener;
	}

	public final Class<? extends EventListener> getObject() {
		return this.object;
	}

	public final Method getMethod() {
		return this.method;
	}
}