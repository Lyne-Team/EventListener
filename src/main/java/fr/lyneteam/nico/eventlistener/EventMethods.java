package fr.lyneteam.nico.eventlistener;

public interface EventMethods {
	public void before();
	public void after();
}