package fr.lyneteam.nico.eventlistener;

import fr.lyneteam.nico.eventlistener.EventHandler.Priority;

public class EventListenerMain extends EventUser {
	public static void main(String[] arguments) {
		EventListenerMain main = new EventListenerMain();
		main.callEvent(new EventListenerMainEvent());
	}
	
	@EventHandler(priority=Priority.HIGHEST)
	public void onEvent(EventListenerMainEvent event) {
		System.out.println("2");
	}
}